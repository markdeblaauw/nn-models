# Make sure it uses the tests rule and not folder.
.PHONY: tests

virtual_env:
	python -m venv venv

# Advised is to have your virtual env activated: source venv/bin/activate
install_package:
	python -m pip install --upgrade pip
	pip install -e .

install_dev_package:
	python -m pip install --upgrade pip
	pip install -e .[dev]

	pre-commit install

tests:
	python -m unittest discover -s tests

pre_commit:
	pre-commit run --all-files

coverage:
	coverage run -m unittest discover -s tests -p "test_*.py" -b

coverage_report:
	coverage run -m unittest discover -s tests -p "test_*.py" -b
	coverage html
	open htmlcov/index.html