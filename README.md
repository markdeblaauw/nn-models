<div id="top"></div>

# nn-models


<!-- ABOUT THE PROJECT -->
## About The Project

This project is is a collection of Neural networks (NNs) written in Pytorch using the [nn-helper](https://pypi.org/project/nn-helper/) library. These are the current models that are now implemented:

- [Arbitrary Style Transfer with Style-Attentional Networks (sanet)](https://arxiv.org/abs/1812.02342)

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

These are the libraries and frameworks with which the library is mainly constructed with:

* [Python](https://python.org/)
* [Pytorch](https://pytorch.org/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started Locally

This section explains how to set-up the repository locally from this repository. Note that this explanation is compatible for a MacOS / Linux environment.

### Prerequisites

1. Have at least Python 3.8 installed on your local machine.
2. Make sure to have [make](https://www.gnu.org/software/make/) installed. Else, you can copy paste commands from the Makefile into your terminal.

### Installation

1. Clone the repo and cd into it
   ```sh
   git clone https://gitlab.com/markdeblaauw/nn-models.git
   ```
3. Create a Python virtual environment
    ```sh
    make virtual_env
    . venv/bin/activate
    ```
4. Install the development Python package
    ```sh
    make install_dev_package
    ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage
This is an example of using this package with the [nn-helper](https://pypi.org/project/nn-helper/) library:

```python
import nn_helper
import torch
from torch.utils.data import DataLoader

import nn_models.sanet as sanet


dataset = sanet.Dataset(
    content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style", random_crop_size=32
)

data_loader = DataLoader(dataset=dataset, batch_size=1, sampler=sanet RandomSampler(dataset))

model = sanet.SanetModel()

loss_fn = sanet.SanetLoss(gamma_weight=5.0)

optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-4)

nn_helper.fit(model=model, data_loader=data_loader, epochs=1, loss_fn=loss_fn, optimizer=optimizer)
```

### Testing

You can run unit tests locally as follows:
```bash
make tests
```

A coverage report can be generated with the intention of 100% coverage:
```bash
make coverage_report
```

<p align="right">(<a href="#top">back to top</a>)</p>

## Roadmap

More models will follow. For now, the focus is heavily on style transfer models.

<p align="right">(<a href="#top">back to top</a>)</p>

