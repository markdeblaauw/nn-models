"""Installation script for the nn_models package."""
from setuptools import find_packages, setup

VERSION = "0.0.1"
DESCRIPTION = "Neural network models implemented with Pytorch"

test = ["coverage>=6.0.2", "pre-commit>=2.15.0"]


setup(
    name="nn_models",
    version=VERSION,
    license="MIT",
    author="Mark de Blaauw",
    author_email="markdeblaauw@gmail.com",
    description=DESCRIPTION,
    url="https://gitlab.com/markdeblaauw/nn-models",
    packages=find_packages(include=["nn_models", "nn_models.*"]),
    install_requires=["torch>=1.9.1", "torchvision>=0.11.3", "numpy>=1.21.2", "Pillow>=9.0.0", "nn_helper>=0.0.1"],
    extras_require={
        "dev": test,
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    keywords=["python", "neural network", "pytorch"],
    python_requires=">=3.8",
)
