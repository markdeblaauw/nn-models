"""Contains utility function related to images."""
from typing import List

from PIL import Image


def is_image(image_path: str, file_extensions: List[str] = [".png", ".jpg", ".jpeg"]) -> bool:
    """Checks whether an image is valid based on its extension.

    Args:
        image_path (str): The path of an image that should be checked.
        file_extensions (List[str], optional): The allowed extensions. Defaults to [".png", ".jpg", ".jpeg"].

    Returns:
        bool: Whether it is a valid image.
    """
    return any(image_path.endswith(extension) for extension in file_extensions)


def resize_image(image: Image.Image, shorter_side: int) -> Image.Image:
    """Resize the image based on a ratio.

    Args:
        image (Image.Image): The image object.
        shorter_side (int): The size that the shorter size should be.

    Returns:
        Image.Image: The resized image.
    """
    width, height = image.size

    if width < height:
        new_width = shorter_side
        new_height = int(height * new_width / width)
    else:
        new_height = shorter_side
        new_width = int(width * new_height / height)

    return image.resize((new_width, new_height), resample=Image.LANCZOS)
