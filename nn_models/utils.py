"""Contains generic utility functions."""
import os
from zipfile import ZipFile


def unzip_files_in_directory(zip_directory_path: str, directory_name: str) -> str:
    """Unzip a zip file into a newly created directory.

    E.g.,:
    zip_directory_path/
        zip_file.zip

    zip_directory_path/
        zip_file.zip
        directory_name/
            image1.jpg
            image2.jpg
            etc

    Args:
        zip_directory_path (str): The path where the zip file is stored.
        directory_name (str): The name for the new directory.

    Returns:
        str: The path to the newly created directory.
    """
    zip_path = os.path.join(zip_directory_path, os.listdir(zip_directory_path)[0])
    new_directory_path = os.path.join(zip_directory_path, directory_name)
    os.mkdir(new_directory_path)

    with ZipFile(zip_path, "r") as zipped_object:
        zipped_object.extractall(path=new_directory_path)

    return new_directory_path
