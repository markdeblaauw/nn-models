"""Contains the loss functions for the sanet model."""
from typing import Dict, Tuple

import torch
import torch.nn as nn

from .utils import calculate_mean_and_std, mean_variance_normalization


class SanetLoss(nn.Module):
    """The sanet loss class."""

    def __init__(self, gamma_weight: float):
        """Initialise the loss parameters.

        Args:
            gamma_weight (float): Described loss parameters from paper.
        """
        super().__init__()
        self.gamma_weight = gamma_weight

    def content_loss(self, inputs: torch.Tensor, targets: torch.Tensor, norm: bool = False) -> float:
        """The loss that makes sure the content is captured in the output.

        Args:
            inputs (torch.Tensor): The content input features.
            targets (torch.Tensor): The target features.
            norm (bool, optional): Whether to normalize the inputs and targets. Defaults to False.

        Returns:
            float: The loss as a number.
        """
        loss = 0
        for input, target in zip(inputs, targets):
            if norm:
                loss += nn.MSELoss()(mean_variance_normalization(input), mean_variance_normalization(target))
            else:
                loss += nn.MSELoss()(input, target)

        return loss

    def style_loss(self, inputs: torch.Tensor, targets: torch.Tensor) -> float:
        """The loss that makes sure the style is captured in the output.

        Args:
            inputs (torch.Tensor): The style input features.
            targets (torch.Tensor): The target features.

        Returns:
            float: The loss as a number.
        """
        loss = 0
        for input, target in zip(inputs, targets):
            input_mean, input_std = calculate_mean_and_std(input)
            target_mean, target_std = calculate_mean_and_std(target)
            loss += nn.MSELoss()(input_mean, target_mean) + nn.MSELoss()(input_std, target_std)

        return loss

    def forward(
        self,
        input_batch: Tuple[torch.Tensor, torch.Tensor],
        style_features: torch.Tensor,
        content_features: torch.Tensor,
        decoder_output_features: Tuple[
            torch.Tensor,
        ],
        Icc: torch.Tensor,
        Iss: torch.Tensor,
        Fcc: torch.Tensor,
        Fss: torch.Tensor,
    ) -> Dict[str, float]:
        """The forward function of the loss.

        Args:
            input_batch (Tuple[torch.Tensor, torch.Tensor]): The original input batch.
            style_features (torch.Tensor): The style features.
            content_features (torch.Tensor): The content features.
            decoder_output_features (Tuple[torch.Tensor,]): The output features from the decoder processed
                through the encoder. It returns features from 5 different layers as a tuple.
            Icc (torch.Tensor): [description]
            Iss (torch.Tensor): [description]
            Fcc (torch.Tensor): [description]
            Fss (torch.Tensor): [description]

        Returns:
            Dict[str, float]: [description]
        """
        content_loss = self.content_loss(inputs=decoder_output_features[2:4], targets=content_features[2:4], norm=True)

        style_loss = self.style_loss(inputs=decoder_output_features, targets=style_features)

        identity1 = self.content_loss(inputs=Icc, targets=input_batch[0]) + self.content_loss(
            inputs=Iss, targets=input_batch[1]
        )

        identity2 = self.content_loss(inputs=Fcc, targets=content_features) + self.content_loss(
            inputs=Fss, targets=style_features
        )

        loss = content_loss + self.gamma_weight * style_loss + identity1 * 50 + identity2 * 1

        return dict(
            loss=loss,
            content_loss=content_loss,
            style_loss=style_loss,
            identity1_loss=identity1,
            identity2_loss=identity2,
        )
