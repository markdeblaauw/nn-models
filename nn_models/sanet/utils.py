"""Contains utility functions for the sanet package."""
from typing import Tuple

import torch


def calculate_mean_and_std(feature: torch.Tensor, eps: float = 1e-5) -> Tuple[torch.Tensor, torch.Tensor]:
    """Calculate the mean and standard deviation of a batch of features.

    The feature should have dimensions [batch, channels, height, width].

    Args:
        feature (torch.Tensor): The feature or batch.
        eps (float, optional): A small value added to the variance to avoid divide-by-zero. Defaults to 1e-5.

    Returns:
        Tuple[torch.Tensor, torch.Tensor]: The feature mean and standard deviation.
    """
    size = feature.size()
    assert len(size) == 4, f"The feature size should be 4 and not {len(size)}."
    batch, channels = size[:2]
    feature_var = feature.view(batch, channels, -1).var(dim=2) + eps
    feature_std = feature_var.sqrt().view(batch, channels, 1, 1)
    feature_mean = feature.view(batch, channels, -1).mean(dim=2).view(batch, channels, 1, 1)
    return feature_mean, feature_std


def mean_variance_normalization(feature: torch.Tensor) -> torch.Tensor:
    """Normalize a feature / batch by mean and variance.

    The feature should have dimensions [batch, channels, height, width].

    Args:
        feature (torch.Tensor): The feature or batch.

    Returns:
        torch.Tensor: The normalized feature.
    """
    size = feature.size()
    mean, std = calculate_mean_and_std(feature)
    normalized_feature = (feature - mean.expand(size)) / std.expand(size)
    return normalized_feature
