"""The logic for the sanet model."""
from typing import Dict, Tuple, Union

import nn_helper
import torch
import torch.nn as nn

from nn_models.vgg.backbones import NormalisedVGG19Backbone

from .utils import mean_variance_normalization


class SanetModel(nn_helper.ModelBase):
    """The main SANET model class."""

    def __init__(self, encoder: nn.Module = None, decoder: nn.Module = None):
        """Initialise the class."""
        super().__init__()
        self.encoder = encoder if encoder else NormalisedVGG19Backbone()
        self.transform = Transform(in_planes=512)
        self.decoder = decoder if decoder else SanetDecoder()

    def forward(self, content: torch.Tensor, style: torch.Tensor) -> Union[torch.Tensor, Tuple[torch.Tensor]]:
        """The forward function.

        Args:
            content (torch.Tensor): The content image(s) as [batch, channels, height, width].
            style (torch.Tensor): The style image(s) as [batch, channels, height, width].

        Returns:
            Union[torch.Tensor, Tuple[torch.Tensor]]: The eval output as a torch tensor or
                tuple output of torch tensors during training.
        """
        style_features = self.encoder(style)
        content_features = self.encoder(content)
        stylized = self.transform(content_features[3], style_features[3], content_features[4], style_features[4])
        decoder_output = self.decoder(stylized)

        if not self.training:
            return decoder_output

        decoder_output_features = self.encoder(decoder_output)

        Icc = self.decoder(
            self.transform(content_features[3], content_features[3], content_features[4], content_features[4])
        )

        Iss = self.decoder(self.transform(style_features[3], style_features[3], style_features[4], style_features[4]))

        Fcc = self.encoder(Icc)
        Fss = self.encoder(Iss)

        return style_features, content_features, decoder_output_features, Icc, Iss, Fcc, Fss

    def fit_function(
        self, input_batch: Tuple[torch.Tensor, torch.Tensor], loss_fn: torch.nn.Module, optimizer: torch.optim.Optimizer
    ) -> Dict[str, torch.Tensor]:
        """Custom fit function.

        Args:
            input_batch (Tuple[torch.Tensor, torch.Tensor]): The training data.
            loss_fn (torch.nn.Module): The loss function that should be used.
            optimizer (torch.optim.Optimizer): The optimizer that should be used.

        Returns:
            Dict[str, torch.Tensor]: The loss value(s).
        """
        self.train()
        optimizer.zero_grad()

        outputs = self(*input_batch)

        losses = loss_fn(input_batch, *outputs)

        losses["loss"].backward()
        optimizer.step()

        return losses


class StyleAttentionalNetwork(nn.Module):
    """Middle computations of the sanet model."""

    def __init__(self, in_planes: int = 512):
        """Initialise the model.

        Args:
            in_planes (int, optional): The number of channels from the encoder.
                Defaults to 512.
        """
        super().__init__()
        self.f = nn.Conv2d(in_planes, in_planes, (1, 1))
        self.g = nn.Conv2d(in_planes, in_planes, (1, 1))
        self.h = nn.Conv2d(in_planes, in_planes, (1, 1))
        self.softmax = nn.Softmax(dim=-1)
        self.out_conv = nn.Conv2d(in_planes, in_planes, (1, 1))

    def forward(self, content: torch.Tensor, style: torch.Tensor) -> torch.Tensor:
        """The forward function.

        Args:
            content (torch.Tensor): The content picture from the encoder.
            style (torch.Tensor): The style picture from the encoder.

        Returns:
            torch.Tensor: The combined content and style output.
        """
        F = self.f(mean_variance_normalization(content))
        G = self.g(mean_variance_normalization(style))
        H = self.h(style)
        b, c, h, w = F.size()
        F = F.view(b, -1, w * h).permute(0, 2, 1)
        b, c, h, w = G.size()
        G = G.view(b, -1, w * h)
        S = torch.bmm(F, G)
        S = self.softmax(S)
        b, c, h, w = H.size()
        H = H.view(b, -1, w * h)
        output = torch.bmm(H, S.permute(0, 2, 1))
        b, c, h, w = content.size()
        output = output.view(b, c, h, w)
        output = self.out_conv(output)
        output += content
        return output


class Transform(nn.Module):
    """Performs twice StyleAttentionalNetwork model."""

    def __init__(self, in_planes: int = 512):
        """Initialise the model.

        Args:
            in_planes (int, optional): The number of channels from the encoder.
                Defaults to 512.
        """
        super(Transform, self).__init__()
        self.sanet4_1 = StyleAttentionalNetwork(in_planes=in_planes)
        self.sanet5_1 = StyleAttentionalNetwork(in_planes=in_planes)
        self.upsample5_1 = nn.Upsample(scale_factor=2, mode="nearest")
        self.merge_conv_pad = nn.ReflectionPad2d((1, 1, 1, 1))
        self.merge_conv = nn.Conv2d(in_planes, in_planes, (3, 3))

    def forward(
        self, content4_1: torch.Tensor, style4_1: torch.Tensor, content5_1: torch.Tensor, style5_1: torch.Tensor
    ) -> torch.Tensor:
        """The forward function.

        Args:
            content4_1 (torch.Tensor): The content from encoder layer relu4_1.
            style4_1 (torch.Tensor): The style from encoder layer relu4_1.
            content5_1 (torch.Tensor): The content from encoder layer relu5_1.
            style5_1 (torch.Tensor): The style from encoder layer relu5_1.

        Returns:
            torch.Tensor: The output of the combined content and styles.
        """
        return self.merge_conv(
            self.merge_conv_pad(
                self.sanet4_1(content4_1, style4_1) + self.upsample5_1(self.sanet5_1(content5_1, style5_1))
            )
        )


class SanetDecoder(nn.Module):
    """The Sanet decoder specified in the paper."""

    def __init__(self):
        """Initialise the model."""
        super().__init__()
        self.net = nn.Sequential(
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(512, 256, (3, 3)),
            nn.ReLU(),
            nn.Upsample(scale_factor=2, mode="nearest"),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, (3, 3)),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, (3, 3)),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, (3, 3)),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 128, (3, 3)),
            nn.ReLU(),
            nn.Upsample(scale_factor=2, mode="nearest"),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(128, 128, (3, 3)),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(128, 64, (3, 3)),
            nn.ReLU(),
            nn.Upsample(scale_factor=2, mode="nearest"),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(64, 64, (3, 3)),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(64, 3, (3, 3)),
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """The forward function.

        Args:
            x (torch.Tensor): The input data.

        Returns:
            torch.Tensor: The output data.
        """
        return self.net(x)
