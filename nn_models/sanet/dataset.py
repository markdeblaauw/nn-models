"""Dataset and sampler for the sanet model."""
import os
from typing import Generator, Tuple

import numpy as np
import torch
import torch.utils.data as data
import torchvision.transforms as transforms
from PIL import Image

from nn_models.image_utils import is_image, resize_image
from nn_models.utils import unzip_files_in_directory


class Dataset(data.Dataset):
    """Data source class.

    This dataset works only together with the RandomSampler class.
    """

    def __init__(
        self,
        content_directory: str,
        style_directory: str,
        shorter_side: int = 300,
        do_resize_image: bool = False,
        random_crop_size: int = 256,
    ):
        """Initialise the data source class.

        Args:
            content_directory (str): The path where the content images are stored. This can be either
                images or a zip file with images.
            style_directory (str): The path where the style images are stored. This can be either
                images or a zip file with images.
            shorter_side (int, optional): Resize the images with the given value on the shortest size,
                while keeping the width / height ratio intact. Defaults to 300.
            do_resize_image (bool, optional): Whether to resize at all. Defaults to False.
            random_crop_size (int, optional): A random crop is taken for the image. This defines
                the size of this random crop. Defaults to 256.
        """
        super().__init__()

        # Check if images are zipped and if so, unzip them in new directory.
        if len(os.listdir(content_directory)) == 1 and os.listdir(content_directory)[0].endswith("zip"):
            print("Start unzipping content data.")
            content_directory = unzip_files_in_directory(zip_directory_path=content_directory, directory_name="content")

        if len(os.listdir(style_directory)) == 1 and os.listdir(style_directory)[0].endswith("zip"):
            print("Start unzipping style data.")
            style_directory = unzip_files_in_directory(zip_directory_path=style_directory, directory_name="style")

        content_list = [
            os.path.join(content_directory, image_name)
            for image_name in os.listdir(content_directory)
            if is_image(image_name)
        ]

        style_list = [
            os.path.join(style_directory, image_name)
            for image_name in os.listdir(style_directory)
            if is_image(image_name)
        ]

        # Make sure that content and style are equal length.
        self.image_pairs = list(zip(content_list, style_list))
        print(f"There are {len(self.image_pairs)} image pairs in the dataset!")

        self.shorter_side = shorter_side
        self.do_resize_image = do_resize_image
        self.transform_list = transforms.Compose(
            [transforms.RandomCrop(random_crop_size), transforms.RandomHorizontalFlip(), transforms.ToTensor()]
        )

    def __getitem__(self, indexes: Tuple[int, int]) -> Tuple[torch.Tensor]:
        """Function to create batches of images.

        Args:
            indexes (Tuple[int, int]): Which content and style that should be returned.

        Returns:
            Tuple[torch.Tensor]: The content and style pair.
        """
        content_image = Image.open(self.image_pairs[indexes[0]][0]).convert("RGB")
        style_image = Image.open(self.image_pairs[indexes[1]][1]).convert("RGB")

        if self.do_resize_image:
            content_image = resize_image(image=content_image, shorter_side=self.shorter_side)
            style_image = resize_image(image=style_image, shorter_side=self.shorter_side)

        content_image = self.transform_list(content_image)
        style_image = self.transform_list(style_image)

        return content_image, style_image

    def __len__(self) -> int:
        """Gives the length of data samples in the data source.

        Returns:
            int: Number of samples.
        """
        return len(self.image_pairs)


class RandomSampler(data.Sampler):
    """Random sampler for pair of images."""

    def __init__(self, dataset: data.Dataset):
        """Initialise class with data source.

        Args:
            dataset (data.Dataset): The data source.
        """
        super().__init__(data_source=dataset)
        self.dataset = dataset
        self.num_samples = len(dataset)

    def __len__(self) -> int:
        """Gives the length of data samples in the data source.

        Returns:
            int: Number of samples.
        """
        return self.num_samples

    def __iter__(self) -> Generator:
        """Samples image pairs.

        This iterator is reinitialised after every epoch.

        Yields:
            Generator: Iterates through sample pairs.
        """
        content_list = np.random.permutation(self.num_samples)
        style_list = np.random.permutation(self.num_samples)
        for i in range(self.num_samples):
            yield content_list[i], style_list[i]
