"""Contains VGG pre-trained backbones.

These backbones can be used in other neural networks.
"""
import os
from typing import Tuple
from urllib.request import urlretrieve

import torch
import torch.nn as nn


class NormalisedVGG19Backbone(nn.Module):
    """A normalised VGG19 Backbone neural network.

    We normalized the network by scaling the weights such that the mean activation
    of each convolutional ﬁlter over images and positions is equal to one.
    Such re-scaling can be done for the VGG network without changing its output,
    because it contains only rectifying linear activation functions and no
    normalization or pooling over feature maps:
    https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf.
    """

    def __init__(
        self,
        require_grad: bool = False,
        pre_trained: bool = True,
        pre_trained_path: str = "pre_trained_weights/vgg_normalised_conv5_1.pth",
    ):
        """Initialise VGG19 backbone.

        Args:
            require_grad (bool, optional): Whether weights should be updated during training.
                Defaults to False.
            pre_trained (bool, optional): Whether to use pre-trained weights. Defaults to True.
            pre_trained_path (str, optional): Optional to use different weights.
                Defaults to "pre_trained_weights/vgg_normalised_conv5_1.pth".
        """
        super().__init__()
        net = nn.Sequential(
            nn.Conv2d(3, 3, 1),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(3, 64, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(64, 64, 3),
            nn.ReLU(),
            nn.MaxPool2d(2, ceil_mode=True),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(64, 128, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(128, 128, 3),
            nn.ReLU(),
            nn.MaxPool2d(2, ceil_mode=True),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(128, 256, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 256, 3),
            nn.ReLU(),
            nn.MaxPool2d(2, ceil_mode=True),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(256, 512, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(512, 512, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(512, 512, 3),
            nn.ReLU(),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(512, 512, 3),
            nn.ReLU(),
            nn.MaxPool2d(2, ceil_mode=True),
            nn.ReflectionPad2d((1, 1, 1, 1)),
            nn.Conv2d(512, 512, 3),
            nn.ReLU(),
        )

        if pre_trained:
            model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), pre_trained_path)
            try:
                net.load_state_dict(torch.load(model_path))
            except FileNotFoundError:
                print("Download VGG19 Weights...")
                urlretrieve(
                    (
                        "https://gitlab.com/markdeblaauw/nn-models/-/raw/dev/"
                        "nn_models/vgg/pre_trained_weights/vgg_normalised_conv5_1.pth"
                    ),
                    model_path,
                )
                net.load_state_dict(torch.load(model_path))

        self.block1 = net[:4]
        self.block2 = net[4:11]
        self.block3 = net[11:18]
        self.block4 = net[18:31]
        self.block5 = net[31:]

        if not require_grad:
            for parameters in self.parameters():
                parameters.requires_grad = False

    def forward(self, x: torch.Tensor) -> Tuple[torch.Tensor]:
        """Model forward function.

        Args:
            x (torch.Tensor): Input values with shape: [batch, channels, height, width]. For which
                channels must be 3.

        Returns:
            Tuple[torch.Tensor]: The model output from 5 different layers of the VGG19 backbone.
        """
        h1 = self.block1(x)
        h2 = self.block2(h1)
        h3 = self.block3(h2)
        h4 = self.block4(h3)
        h5 = self.block5(h4)

        return h1, h2, h3, h4, h5
