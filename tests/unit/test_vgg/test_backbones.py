import unittest
from unittest.mock import patch

import torch

from nn_models.vgg.backbones import NormalisedVGG19Backbone


class TestNormalisedVGG19Backbone(unittest.TestCase):
    def test_model_init_without_weights(self):
        model = NormalisedVGG19Backbone(pre_trained=False)

        input = torch.rand(size=[1, 3, 18, 18])
        model(input)

    def test_model_init_with_weights_called(self):
        with patch("nn_models.vgg.backbones.nn.Sequential") as mock_nn_sequential, patch(
            "nn_models.vgg.backbones.torch.load"
        ) as mock_torch_load:
            NormalisedVGG19Backbone()

            mock_torch_load.assert_called_once()
            mock_nn_sequential.return_value.load_state_dict.assert_called_once_with(mock_torch_load.return_value)

    def test_model_init_with_weights_runs(self):
        model = NormalisedVGG19Backbone()

        input = torch.rand(size=[1, 3, 18, 18])
        model(input)

    def test_forward_model_output_layers(self):
        model = NormalisedVGG19Backbone()
        input = torch.rand(size=[1, 3, 18, 18])

        output = model(input)
        self.assertIsInstance(output[4], torch.Tensor)
        self.assertEqual(len(output), 5)

    def test_model_not_require_grad(self):
        model = NormalisedVGG19Backbone()

        for parameters in model.parameters():
            self.assertFalse(parameters.requires_grad)

    def test_model_require_grad(self):
        model = NormalisedVGG19Backbone(require_grad=True)

        for parameters in model.parameters():
            self.assertTrue(parameters.requires_grad)

    def test_pre_trained_download_weights(self):
        with patch("nn_models.vgg.backbones.urlretrieve") as mock_urlretrieve, patch(
            "nn_models.vgg.backbones.print"
        ) as mock_print:
            self.assertRaises(
                FileNotFoundError, NormalisedVGG19Backbone, require_grad=True, pre_trained_path="random.pth"
            )

            mock_print.assert_called_once_with("Download VGG19 Weights...")
