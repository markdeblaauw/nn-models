import os
import unittest
from tempfile import TemporaryDirectory
from zipfile import ZipFile

from nn_models.utils import unzip_files_in_directory


class TestUtils(unittest.TestCase):
    def test_unzip_files_in_directory(self):
        with TemporaryDirectory() as tmp_dir:
            with ZipFile(os.path.join(tmp_dir, "dummy.zip"), "w") as dummy_zip:
                dummy_zip.write(filename="tests/data/sanet/style/bensindrawing.jpeg", arcname="bensindrawing.jpeg")
                dummy_zip.write(filename="tests/data/sanet/style/starrynight.jpeg", arcname="starrynight.jpeg")

            new_path = unzip_files_in_directory(zip_directory_path=tmp_dir, directory_name="style")
            style_files = os.listdir(new_path)

        self.assertTrue("bensindrawing.jpeg" in style_files)
        self.assertTrue("starrynight.jpeg" in style_files)
        self.assertEqual(len(style_files), 2)
