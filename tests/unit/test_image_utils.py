import unittest

from PIL import Image

from nn_models.image_utils import is_image, resize_image


class TestImageUtils(unittest.TestCase):
    def test_is_image(self):
        output = [
            path
            for path in ["directory/file.jpg", "directory/standard.zip", "dir/x.jpeg", "some.png"]
            if is_image(image_path=path)
        ]
        self.assertEqual(output, ["directory/file.jpg", "dir/x.jpeg", "some.png"])

    def test_resize_image_width_short(self):
        short_width_image = Image.new(mode="RGB", size=(20, 30))

        resized_image = resize_image(image=short_width_image, shorter_side=10)

        self.assertEqual(resized_image.size, (10, 15))

    def test_resize_image_height_short(self):
        short_width_image = Image.new(mode="RGB", size=(30, 20))

        resized_image = resize_image(image=short_width_image, shorter_side=10)

        self.assertEqual(resized_image.size, (15, 10))
