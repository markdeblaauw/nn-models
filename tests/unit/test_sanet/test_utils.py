import unittest

import torch

from nn_models.sanet.utils import calculate_mean_and_std, mean_variance_normalization


class TestUtils(unittest.TestCase):
    def test_correct_calculate_mean_and_std(self):
        input = torch.tensor([[[[2.0, 0.0], [2.0, 0.0]], [[1.0, 0.0], [1.0, 0.0]]]])
        expected_mean = torch.tensor([[[[1.0]], [[0.5]]]])
        expected_std = torch.tensor([[[[1.1547]], [[0.5774]]]])

        output = calculate_mean_and_std(feature=input)

        self.assertTrue(torch.allclose(output[0], expected_mean))
        self.assertTrue(torch.allclose(output[1], expected_std, rtol=0.0001))

    def test_mean_variance_normalization(self):
        input = torch.tensor([[[[2.0, 0.0], [2.0, 0.0]], [[1.0, 0.0], [1.0, 0.0]]]])

        expected_normalised_feature = torch.tensor(
            [[[[0.866, -0.866], [0.866, -0.866]], [[0.866, -0.866], [0.866, -0.866]]]]
        )

        output = mean_variance_normalization(feature=input)

        self.assertTrue(torch.allclose(output, expected_normalised_feature, rtol=0.0001))

    def test_assert_raise_calculate_mean_and_std(self):
        input = torch.tensor([[[2.0, 0.0], [2.0, 0.0]], [[1.0, 0.0], [1.0, 0.0]]])

        self.assertRaises(AssertionError, calculate_mean_and_std, input)
