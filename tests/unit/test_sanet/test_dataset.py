import os
import unittest
from tempfile import TemporaryDirectory
from unittest.mock import MagicMock, patch
from zipfile import ZipFile

import torch
import torch.utils.data as data

from nn_models.sanet.dataset import Dataset, RandomSampler


class TestDataset(unittest.TestCase):
    def test_dataset_init_with_folder_pairs(self):
        with patch("nn_models.sanet.dataset.print") as mock_print:
            dataset = Dataset(content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style")

            mock_print.assert_called_once_with("There are 1 image pairs in the dataset!")

        self.assertEqual(dataset.shorter_side, 300)
        self.assertEqual(dataset.do_resize_image, False)
        self.assertEqual(len(dataset.image_pairs), 1)

    def test_dataset_init_with_zip_pairs(self):
        with TemporaryDirectory() as tmp_dir, patch("nn_models.sanet.dataset.print") as mock_print:
            os.mkdir(path=os.path.join(tmp_dir, "content"))
            os.mkdir(path=os.path.join(tmp_dir, "style"))
            with ZipFile(os.path.join(tmp_dir, "content", "content.zip"), "w") as content_zip, ZipFile(
                os.path.join(tmp_dir, "style", "style.zip"), "w"
            ) as style_zip:
                content_zip.write(filename="tests/data/sanet/content/face.jpg", arcname="face.jpg")
                style_zip.write(filename="tests/data/sanet/style/starrynight.jpeg", arcname="starrynight.jpeg")
                style_zip.write(filename="tests/data/sanet/style/bensindrawing.jpeg", arcname="bensindrawing.jpeg")

            dataset = Dataset(
                content_directory=os.path.join(tmp_dir, "content"), style_directory=os.path.join(tmp_dir, "style")
            )

            mock_print.assert_called_once_with("There are 1 image pairs in the dataset!")

            self.assertEqual(dataset.shorter_side, 300)
            self.assertEqual(dataset.do_resize_image, False)
            self.assertEqual(len(dataset.image_pairs), 1)

    def test_dataset_get_length(self):
        dataset = Dataset(content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style")

        self.assertEqual(len(dataset), 1)

    def test_dataset_get_item_without_resizing(self):
        with patch("nn_models.sanet.dataset.resize_image") as mock_resize_image:

            dataset = Dataset(content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style")
            pairs = dataset[(0, 0)]

            mock_resize_image.assert_not_called()
            self.assertIsInstance(pairs[0], torch.Tensor)
            self.assertIsInstance(pairs[1], torch.Tensor)

    def test_dataset_get_item_with_resizing(self):
        with patch("nn_models.sanet.dataset.resize_image") as mock_resize_image:

            dataset = Dataset(
                content_directory="tests/data/sanet/content",
                style_directory="tests/data/sanet/style",
                do_resize_image=True,
            )

            dataset.transform_list = MagicMock()
            dataset[(0, 0)]

            mock_resize_image.assert_called()

    def test_dataset_get_item_with_correct_size(self):
        with patch("nn_models.sanet.dataset.resize_image") as mock_resize_image:

            dataset = Dataset(content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style")
            pairs = dataset[(0, 0)]

            self.assertEqual(pairs[0].shape, (3, 256, 256))


class TestRandomSampler(unittest.TestCase):
    def setUp(self) -> None:
        class DummyDataset(data.Dataset):
            def __init__(self) -> None:
                self.len_samples = 5

            def __len__(self):
                return self.len_samples

            def __getitem__(self, index):
                return index

        self.dummy_dataset = DummyDataset()

    def test_length_dunder_method(self):
        sampler = RandomSampler(dataset=self.dummy_dataset)
        self.assertEqual(len(sampler), 5)

    def test_generator(self):
        sampler = RandomSampler(dataset=self.dummy_dataset)
        pairs = [sample_pair_index for sample_pair_index in sampler]

        self.assertEqual(len(pairs), 5)
        self.assertIsInstance(pairs[0], tuple)
        self.assertEqual(len(pairs[0]), 2)
