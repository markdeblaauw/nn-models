import unittest

from torch.utils.data import DataLoader

from nn_models.sanet.dataset import Dataset, RandomSampler


class TestDataset(unittest.TestCase):
    def test_dataset_and_random_sampler_with_dataloader(self):
        dataset = Dataset(content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style")

        data_loader = DataLoader(dataset=dataset, batch_size=1, sampler=RandomSampler(dataset))

        for batch in data_loader:
            self.assertEqual(batch[0].shape, (1, 3, 256, 256))
            self.assertEqual(batch[1].shape, (1, 3, 256, 256))
