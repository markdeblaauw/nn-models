import unittest

import nn_helper
import torch
from torch.utils.data import DataLoader

import nn_models.sanet as sanet
from nn_models.sanet.model import SanetModel


class TestSanetModel(unittest.TestCase):
    def test_training_sanet_model(self):
        model = SanetModel()

        batch_content = torch.rand([2, 3, 57, 57])
        batch_style = torch.rand([2, 3, 57, 57])

        output = model(content=batch_content, style=batch_style)

        self.assertEqual(len(output), 7)

    def test_eval_sanet_model(self):
        model = SanetModel()
        model.eval()

        batch_content = torch.rand([2, 3, 57, 57])
        batch_style = torch.rand([2, 3, 57, 57])

        output = model(content=batch_content, style=batch_style)

        self.assertIsInstance(output, torch.Tensor)

    def test_sanet_with_nn_helper(self):
        dataset = sanet.Dataset(
            content_directory="tests/data/sanet/content", style_directory="tests/data/sanet/style", random_crop_size=32
        )

        data_loader = DataLoader(dataset=dataset, batch_size=1, sampler=sanet.RandomSampler(dataset))

        model = sanet.SanetModel()

        loss_fn = sanet.SanetLoss(gamma_weight=5.0)

        optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-4)

        nn_helper.fit(model=model, data_loader=data_loader, epochs=1, loss_fn=loss_fn, optimizer=optimizer)
