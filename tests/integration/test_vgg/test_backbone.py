import os
import unittest
from tempfile import TemporaryDirectory
from unittest.mock import patch

from nn_models.vgg.backbones import NormalisedVGG19Backbone


class TestBackbone(unittest.TestCase):
    def test_pre_trained_weight_download(self):
        with TemporaryDirectory() as tmp_directory, patch("nn_models.vgg.backbones.os.path.join") as mock_os_path_join:
            mock_os_path_join.return_value = f"{tmp_directory}/model.pth"
            NormalisedVGG19Backbone(pre_trained_path="random.pth")

            self.assertTrue(os.path.isfile(f"{tmp_directory}/model.pth"))
